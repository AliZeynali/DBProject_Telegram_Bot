import psycopg2
import sys

hostname = 'localhost'
username = 'postgres'
password = '137555'
database = 'DBProject'


def getCurrent(chat_id):
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Select Count(chatId) from userInfo" + " Where chatID = " + str(chat_id)
    cur.execute(sql)
    count = cur.fetchall()
    if int(count[0][0]) == 0:
        sql = "insert into userInfo values ( " + str(chat_id) + " , " + " \'MainMenu\'" + " , null, null )"
        cur.execute(sql)
        myConnection.commit()
        myConnection.close()
        return "MainMenu"
    else:
        sql = "Select current from userInfo" + " Where chatID = " + str(chat_id)
        cur.execute(sql)
        current = cur.fetchall()[0][0]
        myConnection.close()
        return current


def setCurrent(chat_id, newCurrent):
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Update userInfo Set current = \'" + newCurrent + "\'" + " Where chatId = " + str(chat_id)
    cur.execute(sql)
    myConnection.commit()
    myConnection.close()


def updateFoodType(chat_id, newFoodType):
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Update userInfo Set foodType = \'" + newFoodType + "\'" + " Where chatID = " + str(chat_id)
    cur.execute(sql)
    myConnection.commit()
    myConnection.close()


def updateRestaurant(chat_id, newRes):
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Update userInfo Set Res = \'" + newRes + "\'" + " Where chatId = " + str(chat_id)
    cur.execute(sql)
    myConnection.commit()
    myConnection.close()


def giveBranchAddresses(FoodType, Res):
    String = "شعبه های این رستوران در آدرس های زیر قرار دارند:\n"
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Select Street , Alley, town, No , floor from Address where AddressID in ( Select AddressID from Branch where RestaurantID in (Select RestaurantID from Restaurant where Name = \'" + \
          str(Res) + "\'))"
    cur.execute(sql)
    for street, alley, town, No, floor in cur.fetchall():
        address = "Town: " + str(town) + ", Street: " + str(street) + ", alley: " + str(alley) + ", No.: " + str(
            No) + ", Floor: " + str(floor)
        String += "آدرس:" + "\n" + str(address) + "\n"

    if String == "شعبه های این رستوران در آدرس های زیر قرار دارند:\n":
        return "شعبه ای یافت نشد!\n" + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    else:
        return String + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"


def giveInfo(chatID):
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Select foodType, res from userInfo Where chatID  = " + str(chatID)
    cur.execute(sql)
    for foodType, res in cur.fetchall():
        FoodType = foodType
        Res = res
        # print("HEREEEE: " + str(res))
        return FoodType, Res


def giveMenuData(FoodType, Res):
    print(str(Res))
    String = "منو این رستوران به صورت زیر است:\n"
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Select Name,price from Food where restaurantid in (Select restaurantid from restaurant where name = \'" + str(
        Res) + "\')"
    cur.execute(sql)
    for food, price in cur.fetchall():
        String += str(food) + "          " + str(price) + "\n"

    if String == "منو این رستوران به صورت زیر است:\n":
        return "چنین رستورانی یافت نشد!\n" + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    else:
        return String + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"


def giveAddressData(town):
    String = "رستوران های موجود در این محله عبارت است از:\n"
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Select Name, Street, Alley, Town, No, Floor from restaurant, Address, Branch where " \
          "restaurant.restaurantID = Branch.restaurantID and branch.addressID = address.addressid " \
          "and Town = \'" + str(town) + "\'"
    cur.execute(sql)
    for resName, Street, Alley, Town, No, Floor in cur.fetchall():
        address = "Town: " + str(town) + ", Street: " + str(Street) + ", alley: " + str(Alley) + ", No.: " + str(
            No) + ", Floor: " + str(Floor)
        String += "نام رستوران: " + str(resName) + "\n"
        String += "آدرس:" + "\n" + str(address) + "\n\n"

    if String == "رستوران های موجود در این محله عبارت است از:\n":
        return "رستورانی در این محله یافت نشد!\n" + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    else:
        return String + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"


def giveOrderData(NID):
    try:
        x = int(NID)
    except ValueError:
        return "سفارشی یافت نشد!\n" + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    String = "سفارش های شما عبارت بوده است از:\n"
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "select time, date, orders.orderID ,Restaurant.name RName, rating, food.name, food.price from " \
          "orders, orfo, food,Branch, Restaurant where cid =" + str(NID) + " and orfo.orderID = orders.orderID" \
                                                                           " and orfo.foodId = food.foodid and orders.branchId = Branch.branchID and branch.restaurantID = Restaurant.RestaurantID " \
                                                                           "order by orderId ASC"
    cur.execute(sql)
    oID = -1
    for time, date, orderID, RName, rating, FName, FPrice in cur.fetchall():
        if oID != orderID:
            String += 30 * "-" + "\n"
            String += "زمان: " + str(time) + "\n"
            String += "تاریخ: " + str(date) + "\n"
            String += "رستوران: " + str(RName) + "\n"
            String += "نمره داده شده به سفارش توسط کاربر: " + str(rating) + "\n"
            String += "لیست سفارش: " + "\n"
            oID = orderID
        String += "نام غذا: " + str(FName) + ", قیمت: " + str(FPrice) + "\n"

    if String == "سفارش های شما عبارت بوده است از:\n":
        return "سفارشی یافت نشد!\n" + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    else:
        return String + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"


def ResFood(chatID):
    String = "رستوران مورد نظر خود را انتخاب کنید. \n رستوران های دارای این نوع غذا عبارت اند از:\n"
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    sql = "Select foodType from userInfo Where chatId  = " + str(chatID)
    cur.execute(sql)
    for foodType in cur.fetchall():
        FoodType = foodType[0]
    sql = "Select name from Restaurant where RestaurantId in (select RestaurantId from food where fType = \'" + str(
        FoodType) + "\')"
    cur.execute(sql)
    for resName in cur.fetchall():
        String += "رستوران: " + "\n /" + str(resName[0]) + "\n"

    if String == "رستوران مورد نظر خود را انتخاب کنید. \n رستوران های دارای این نوع غذا عبارت اند از:\n":
        return "رستورانی یافت نشد!\n" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    else:
        return String + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"


def tm():
    String = "منو این رستوران به صورت زیر است:\n"
    global hostname, username, password, database
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = myConnection.cursor()
    # sql = "Select res from userInfo Where chatId  = " + str(chatID)
    # cur.execute(sql)
    # for res in cur.fetchall():
    Res = "Bood"
    sql = "Select Name,price from Food where restaurantid in (Select restaurantid from restaurant where name = \'" + str(
        Res) + "\')"
    cur.execute(sql)
    for food, price in cur.fetchall():
        String += str(food) + "          " + str(price) + "\n"

    if String == "منو این رستوران به صورت زیر است:\n":
        return "چنین رستورانی یافت نشد!\n" + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"
    else:
        return String + "\n بازگشت به صفحه اصلی" + "\n/MainMenu" + "\nDesigned By Ali Zeynali & Omid Chehregosha. \n"

        # print(giveOrderData(1111111111))
