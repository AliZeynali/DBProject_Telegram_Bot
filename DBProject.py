import requests
import time
import telepot
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
from dataBases import *

markupStart = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text="جستجو بر اساس نوع غذا")],
                                            [KeyboardButton(text="جستجو بر اساس آدرس")],
                                            [KeyboardButton(text="لیست سفارش های هر شخص")]])
markupFood = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text="Fastfood")],
                                           [KeyboardButton(text="Irani")], [KeyboardButton(text="Italian")],
                                           [KeyboardButton(text="Sonnati")], [KeyboardButton(text="Sokhari")],
                                           [KeyboardButton(text="بازگشت به صفحه اصلی")]])
markupInfoType = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text="آدرس شعبه ها")],
                                               [KeyboardButton(text="منو")],
                                               [KeyboardButton(text="بازگشت به صفحه اصلی")]])

markUpBack = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text="بازگشت به صفحه اصلی")]])
def update_Order(chatID, fType, Res):
    if fType != -1:
        updateFoodType(chatID, fType)
    if Res != -1:
        updateRestaurant(chatID, Res)


def nextMarkUp(nextMark, chat_id):
    global markupStart, markupFood
    current = getCurrent(chat_id)
    if nextMark in {"/start", "start", "MainMenu", "بازگشت به صفحه اصلی"}:
        markup = markupStart
        setCurrent(chat_id, 'MainMenu')
        update_Order(chat_id, "null", "null")
        return markup, "MainMenu", None, None
    if current in {"/start", "MainMenu"}:
        update_Order(chat_id, "null", "null")
        if nextMark == "جستجو بر اساس نوع غذا":
            markup = markupFood
            setCurrent(chat_id, "FoodType")
            return markup, "FoodType", None, None
        elif nextMark == "جستجو بر اساس آدرس":
            setCurrent(chat_id, "Address")
            return markUpBack, "Address", None, None
        elif nextMark == "لیست سفارش های هر شخص":
            setCurrent(chat_id, "Orders")
            return markUpBack, "Id", None, None
    if current == "FoodType":
        if nextMark in {"Fastfood", "Irani", "Italian", "Sonnati", "Sokhari"}:
            update_Order(chat_id, nextMark, -1)
            setCurrent(chat_id, "Restaurant")
            return markUpBack, "Restaurant", None, None
    if current == "Address":
        setCurrent(chat_id, 'MainMenu')
        update_Order(chat_id, "null", "null")
        return markupStart, "AddressData", None, None
    if current == "Restaurant":
        setCurrent(chat_id, "InfoType")
        update_Order(chat_id, -1, nextMark)
        return markupInfoType, "InfoType", None, None
    if current == "InfoType":
        setCurrent(chat_id, 'MainMenu')
        FoodType, Res = giveInfo(chat_id)
        update_Order(chat_id, "null", "null")
        if nextMark == "آدرس شعبه ها":
            return markupStart, "branchAddress", FoodType, Res
        if nextMark == "منو":
            return markupStart, "menuData", FoodType, Res
    if current == "Orders":
        setCurrent(chat_id, 'MainMenu')
        update_Order(chat_id, "null", "null")
        return markupStart, "orderData", None, None
    return None, None, None, None


def on_chat_message(message):
    global current
    content_type, chat_type, chat_id = telepot.glance(message)
    # print(message)
    if content_type == 'text':
        text = message['text']
        text = text.replace("/", "")
        markup, stuff , FoodType, Res= nextMarkUp(text, chat_id)
        # print(markup)
        # print("text: " + text)
        # print(10 * '*')
        if text == 'start':
            setCurrent(chat_id, 'MainMenu')
            markup, stuff, FoodType, Res = nextMarkUp(text, chat_id)
            bot.sendMessage(chat_id, "سلام!", reply_markup=markup)
        elif markup == None and stuff == None:
            bot.sendMessage(chat_id, "Unvalid!", reply_markup=markup)


        elif markup != None:
            if stuff == "MainMenu":
                bot.sendMessage(chat_id, "لطفا نوع جستجو خود را انتخاب کنید: ", reply_markup=markup)
            elif stuff == "FoodType":
                bot.sendMessage(chat_id, "نوع غذای مورد نظر خود را انتخاب کنید.", reply_markup=markup)
            elif stuff == "InfoType":
                bot.sendMessage(chat_id, "مایل به دریافت آدرس شعبه ها هستید یا میخواهید منوی رستوران را مشاهده کنید؟ ",
                                reply_markup=markup)
            elif stuff == "AddressData":
                bot.sendMessage(chat_id, giveAddressData(text), reply_markup=markup)
            elif stuff == "branchAddress":
                bot.sendMessage(chat_id, giveBranchAddresses(FoodType, Res),reply_markup=markup)
            elif stuff == "menuData":
                bot.sendMessage(chat_id, giveMenuData(FoodType, Res), reply_markup=markup)
            elif stuff == "orderData":
                bot.sendMessage(chat_id, giveOrderData(text), reply_markup=markup)
            elif stuff == "Address":
                msg = "محله مورد نظر خود را وارد کنید."
                bot.sendMessage(chat_id, msg,reply_markup=markup)
            elif stuff == "Id":
                msg = "لطفا ID خود را وارد کنید." + "\n\n" + "بازگشت به صفحه اول " + "/MainMenu"
                bot.sendMessage(chat_id, msg,reply_markup=markup)
            elif stuff == "Restaurant":
                msg = ResFood(chat_id)
                bot.sendMessage(chat_id, msg,reply_markup=markup)


token = "392382512:AAEr31MkcXuC4C_Uuh-qn2HTOIF47mbOqbE"
bot = telepot.Bot(token)

bot.message_loop({'chat': on_chat_message})

while True:
    time.sleep(10)
